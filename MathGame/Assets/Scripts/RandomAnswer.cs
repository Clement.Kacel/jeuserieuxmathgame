using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class RandomAnswer : MonoBehaviour
{
    public CubeScript[] cubes;

    public void AssignCubes()
    {
        MathsEquationGenerator multiplicationGenerator = FindObjectOfType<MathsEquationGenerator>();
        multiplicationGenerator.OnMultiplicationResult.AddListener(AssignCorrectAnswer);

    }

    private void AssignCorrectAnswer(int multiplicationResult)
    {
        int correctCubeIndex = Random.Range(0, cubes.Length);

        for (int i = 0; i < cubes.Length; i++)
        {
            if (i == correctCubeIndex)
            {
                cubes[i].AssignAnswer(multiplicationResult);
            }
            else
            {
                cubes[i].AssignWrongAnswer();
            }
        }
    }
}
