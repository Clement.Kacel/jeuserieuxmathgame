using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CubeScript : MonoBehaviour
{
    public TMP_Text answerText;

   
    public void AssignAnswer(int answer)
    {
        answerText.text = answer.ToString();
        gameObject.tag = "CorrectCube";
        

    }

    public void AssignWrongAnswer()
    {
        int wrongAnswer = Random.Range(10, 81);
        answerText.text = wrongAnswer.ToString();
        gameObject.tag = "WrongCube";
       

    }
}

