using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private CharacterController controller;
    public GameObject playerGameObject;
    private Vector3 direction;
    public float forwardSpeed = 10f;
    public float slowedSpeed;
    public float minimumSpeed = 6f;
    private float currentSpeed;
    public static int numberOfCoins;
    public Text coinsText; 
    
    private int desiredLane = 1; // 0 left; 1 middle; 2 right
    public float laneDistance = 2.5f; // distance between lanes
    public float jumpForce;
    public float Gravity = -20;
   
    
    void Start()
    {
        controller = GetComponent<CharacterController>();
        currentSpeed = forwardSpeed;
        numberOfCoins =0;
    }

    // Update is called once per frame
    void Update()
    {
        currentSpeed = currentSpeed;
        direction.z = currentSpeed;
       
       

        if(controller.isGrounded){
        direction.y=-1;
        if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Jump();
            }
       }else{
            direction.y += Gravity * Time.fixedDeltaTime;
       }

        



       if(Input.GetKeyDown(KeyCode.RightArrow)){
        desiredLane++;
        if(desiredLane ==3){
            desiredLane =2;
        }
       }

       if(Input.GetKeyDown(KeyCode.LeftArrow)){
        desiredLane--;
        if(desiredLane ==-1){
            desiredLane =0;
        }
       }

        Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;
        if(desiredLane==0){
            targetPosition += Vector3.left*laneDistance;
        }else if(desiredLane ==2){
            targetPosition += Vector3.right * laneDistance;
        }

        transform.position = Vector3.Lerp(transform.position, targetPosition, 80 * Time.fixedDeltaTime);
        controller.center = controller.center;


        coinsText.text = "Or: " + numberOfCoins;
    }
    private void FixedUpdate(){
        controller.Move(direction*Time.fixedDeltaTime); 
    }

    private void Jump(){
        direction.y = jumpForce;

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CorrectCube"))
        {
            currentSpeed = currentSpeed + 1f;
            Debug.Log("OnTriggerEnter - CorrectCube: Speed increased to " + currentSpeed);
        }
        else if (other.CompareTag("WrongCube"))
        {
            currentSpeed = Mathf.Max(currentSpeed - 3f, minimumSpeed);
            Debug.Log("OnTriggerEnter - WrongCube: Speed decreased to " + currentSpeed);
            FindObjectOfType<AudioManager>().PlaySound("GameOver");
        }
    }



}