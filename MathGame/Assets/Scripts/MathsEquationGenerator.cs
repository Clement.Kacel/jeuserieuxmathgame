using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
public class MathsEquationGenerator : MonoBehaviour
{
    public TMP_Text textField;
    public UnityEvent<int> OnMultiplicationResult;
    private void Start()
    {
        int operand1 = Random.Range(2, 10);
        int operand2 = Random.Range(2, 10);
        int result = operand1 * operand2;
        string newText = "    "+operand1 + " x " + operand2;
        textField.text = newText;
        OnMultiplicationResult.Invoke(result);
    }

    private void Update()
    {
        
      
    }

        
    

}
