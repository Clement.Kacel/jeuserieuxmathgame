using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    public GameObject[] tilePrefabs;
    
    private GameObject spawnedTile;

    public float zSpawn = 0;
    public int numberOfTiles = 2;
    public float tileLength =100;
    
    public Transform playerTransform;
    // Start is called before the first frame update
    void Start()
    {
        for (int i=0; i<numberOfTiles;i++){
            SpawnTile(0);
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if(playerTransform.position.z>zSpawn-(numberOfTiles*tileLength)){
            SpawnTile(0);
        }
    }
    public void SpawnTile(int tileIndex){
        spawnedTile = Instantiate(tilePrefabs[tileIndex], transform.forward * zSpawn, transform.rotation);
        RandomAnswer randomAnswer = spawnedTile.GetComponentInChildren<RandomAnswer>();
        
        randomAnswer.AssignCubes();
        zSpawn += tileLength;
    }
}
